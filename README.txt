To use the module, you just have to implement this hook :


function hook_per_user_mailer($op, $uid=0) {
  switch ($op) {
    case 'time_to_send' :
      //here you set the conditions required to send the mailing
      return $is_it_time_to_send; //boolean
    case 'subscribed' :
      //here you check if the user defined by $uid should receive the mailing
      return $user_subscribed_to_this_mailing; //boolean
    case 'mail' :
      //here you build the mail content according to the user who will receive it
      return $mail; //array('subject' => string, 'body' => string)
    case 'limit' :
      //the number of users to send mails at each cron run
      return $limit; //integer
  }
}